// Copyright 2022 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14
import QtQuick.Window 2.10

ApplicationWindow {
    id: rootWin
        visible: true
        width: 1920
        height: 1000

    Scene3D {
        id: toothScene3D

        property string toothNumb: "1"
        property string primaryObjSource: "qrc:/3dModels/tooth2.obj"

        property bool mirrorMe: false

        anchors.fill: parent


        transform: [
          Matrix4x4 {
            matrix: Qt.matrix4x4(toothScene3D.mirrorMe ? -1 : 1, 0, 0, 0,
                                 0, 1, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1)
          },
          Translate {
            x: toothScene3D.mirrorMe ? toothScene3D.width : 1
          }
        ]

        onToothNumbChanged:  {
            var intValOfTooth = parseInt(toothNumb);
            if(isNaN(intValOfTooth)) { //pediatric

                if( (toothNumb == "A") ||
                        (toothNumb == "B") ||
                        (toothNumb == "C") ||
                        (toothNumb == "D") ||
                        (toothNumb == "E") ||
                        (toothNumb == "P") ||
                        (toothNumb == "R") ||
                        (toothNumb == "S") ||
                        (toothNumb == "T") ){
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth"+toothNumb+".obj";;
                }
                else if(toothNumb == "Q") {
                    toothScene3D.primaryObjSource = "qrc:/3dModels/toothP.obj";
                }
                else if( (toothNumb == "O") ||
                        (toothNumb == "N") ) {
                    toothScene3D.primaryObjSource = "qrc:/3dModels/toothP.obj";
                    mirrorMe = true;
                }
                else {
                    mirrorMe = true;
                    if(toothNumb == "F") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothE.obj";
                    }
                    else if(toothNumb == "G") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothD.obj";
                    }
                    else if(toothNumb == "H") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothC.obj";
                    }
                    else if(toothNumb == "I") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothB.obj";
                    }
                    else if(toothNumb == "J") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothA.obj";
                    }
                    else if(toothNumb == "K") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothT.obj";
                    }
                    else if(toothNumb == "L") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothS.obj";
                    }
                    else if(toothNumb == "M") {
                        toothScene3D.primaryObjSource = "qrc:/3dModels/toothR.obj";
                    }
                }
            }
            else { //adult
                if((intValOfTooth === 23) || (intValOfTooth === 26)) {
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth25.obj";
                }
                else if(intValOfTooth >= 1 && intValOfTooth <= 8) {
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth"+toothNumb+".obj";
                }
                else if(intValOfTooth >= 9 && intValOfTooth <= 16) {
                    var newTop = 17-intValOfTooth;
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth"+newTop+".obj";
                    mirrorMe = true;
                }
                else if(intValOfTooth >= 17 && intValOfTooth <= 24) {
                    var from25 = 25- intValOfTooth ;
                    var newBottom = 24+from25;
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth"+newBottom+".obj";
                    mirrorMe = true;

                }
                else {
                    toothScene3D.primaryObjSource = "qrc:/3dModels/tooth"+toothNumb+".obj";
                }
            }
        }

        Entity {
            id: sceneRoot



            Camera {
                id: camera
                projectionType: CameraLens.PerspectiveProjection
                fieldOfView: 45
                aspectRatio: toothScene3D.width / toothScene3D.height
                nearPlane : 0.1
                farPlane : 1000.0
                position: Qt.vector3d( 0.0, 0.0, -40.0 )
                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
            }


            Entity {
                components: [
                    DirectionalLight {
                        id: dLight
                        color: "white"
                        intensity: .4
                        worldDirection: Qt.vector3d(0,0,1);

                    }]
            }

            components: [
                RenderSettings {
                    activeFrameGraph: ForwardRenderer {
                        clearColor: Qt.rgba(0, 0.0, 0, 0)
                        camera: camera
                    }
                }
            ]

            PhongMaterial {
                property string materialColor: "white"
                property color makeColor: whiteRGBA
                id: material
                ambient:  makeColor
                diffuse: makeColor
                specular: makeColor

                onMaterialColorChanged: {
                    if(materialColor === "white") {
                        makeColor = whiteRGBA
                    }
                    else if(materialColor === "red") {
                        makeColor = redRGBA
                    }
                    else if(materialColor === "green") {
                        makeColor = greenRGBA
                    }
                }

                property color redRGBA: Qt.rgba( .8,0, 0, 1);
                property color greenRGBA: Qt.rgba( 0,.8, 0, 1);
                property color whiteRGBA: Qt.rgba( .8,.8, .8, 1);
            }

            Mesh {
                id: toothMesh
                source:  "qrc:/3dModels/toothT.obj"
                //meshName: "M"
            }

            Transform {
                id: wholeToothTransform

                PropertyAnimation {
                    duration: 1000
                    from: 180
                    to: (180+360)
                    loops: Animation.Infinite
                    target: wholeToothTransform
                    property: "rotationY"
                    running: true
                }
            }

            Entity {
                id: toothEntity
                components: [ toothMesh, material, wholeToothTransform ]
            }
        }
    }


}

